/**
  @file place.js
 
  @date 2019-8
  @author Génesis García Morilla <gengns@gmail.com>

  @description Place
 */
const place = (() => {
  const bar_ = $('#progress-bar')

  let ol_ // Open Layers map
  let layerVector_ // layer

  /**
   * Loads view
   */
  function load() {
    // Template
    const html =
    `<aside>
      <section>
        <img src="img/shopping.jpg">
        <h3>${T_('Markets')}</h3>
        <p>${sessionStorage.user}</p>
      </section>  
      <nav>
        <a><i>accessibility_new</i>${T_('My Markets')}</a>
        <a><i>add_shopping_cart</i>${T_('Buy')}</a>
        <a><i>gavel</i>${T_('Sell')}</a>
        <a><i>shopping_basket</i>${T_('Share Basket')}</a>
        <a><i>card_giftcard</i>${T_('Earnings')}</a>
        <hr>
        <a><i>live_help</i>${T_('Help')}</a>
        <a id="logout"><i>directions_run</i>${T_('Logout')}</a>
      </nav>
    </aside>

    <header>
      <button id="menu"><i>menu</i></button>
      <h2>${T_('Urban Markets')}</h2>
      <button id="findme"><i>place</i></button>
      <button id="search"><i>search</i></button>
    </header>

    <input placeholder="${T_('city')}" autofocus>

    <main id="map"></main>

    <div id="info"></div>

    <div id="blackout"></div>
    `


    // Render
    $('#app').innerHTML = `<div id="place">${html}</div>`


    // Classes
    $$('i').forEach($i => $i.classList.add('material-icons'))


    // Selectors
    const $aside = $('aside')
    const $search = $('#search')
    const $input = $('input')
    const $info = $('#info')
    const $blackout = $('#blackout')


    // Drawer
    $('#menu').onclick = () => // show
      [$aside, $blackout].forEach($e => $e.classList.add('show'))
    
    $blackout.onclick = () => // hide
      [$aside, $blackout].forEach($e => $e.classList.remove('show'))

    
    // Show / hide search input 
    $search.onclick = () => {
      $input.classList.toggle('show')
      $input.classList.contains('show') ? $input.focus() : $input.blur()
    }


    // Search
    $input.onkeyup = e => {
      if (e.key != 'Enter') return;
      bar_.classList.add('show')

      fetch(`${MARKETS_URL}cities.json`)
      .then(response => response.json())
      .then(data => {
        const city = $input.value.toLowerCase().trim()
        $input.blur()

        if (!data[city]) return alert(T_('Sorry, that city name is not available'))
        // Remove previous markets
        layerVector_.getSource().clear()
        // Get new city markets
        fetch_markets(city)
      })
      .catch(err => alert(`${T_('Fetch error')} ${err}`))
      .finally(() => bar_.classList.remove('show'))
    }

    
    // GPS
    $('#findme').onclick = () => {
      bar_.classList.add('show')
      gps(
        (lat, lon) => {
          // Me   
          let feature = layerVector_.getSource().getFeatures()
            .find(f => f.getId() == 'me')
    
          if (!feature) {
            feature = new ol.Feature()
            feature.setId('me')
          }

          feature.set('title', 'Myself')
          feature.set('location', 'Lost')
          feature.set('gps', [lon, lat].join(', '))

          // Where am I
          feature.setGeometry(new ol.geom.Point(
            ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:900913')))

          // 10 km from me
          const circle = 
            new ol.geom.Circle(feature.getGeometry().getCoordinates(), 1e4)
          
          // Markets
          let features = []
          layerVector_.getSource().forEachFeatureIntersectingExtent(
            circle.getExtent(), f => {
              features.push(f)
            })

          // Filter 
          layerVector_.getSource().clear()
          if (features.length) layerVector_.getSource().addFeatures(features)

          // Add me
          layerVector_.getSource().addFeature(feature)

          // View
          ol_.getView().fit(feature.getGeometry().getExtent())
          ol_.getView().setZoom(14)

          bar_.classList.remove('show')
        },
        err => {
          bar_.classList.remove('show')
          if (err === err.PERMISSION_DENIED)
            return alert(T_('Allow GPS access'))
          else 
            return alert(T_('GPS not avaliable, did you switch on?'))
        }
      )
    }

    // Logout
    $('#logout').onclick = () => login.load()


    // Map
    ol_ = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM() // we can use Mapbox with an API key
        })
      ],
      view: new ol.View({ // GPS
        center: ol.proj.fromLonLat([52.498508, 13.383549].reverse()),
        zoom: 4
      })
    })


    // Layer with Markets
    layerVector_ = new ol.layer.Vector({
      source: new ol.source.Vector(),
      style: get_base_style,
    });
    ol_.addLayer(layerVector_)


    // Select
    const select = new ol.interaction.Select({
      condition: ol.events.condition.singleClick,
      style: get_base_style
    });
    ol_.addInteraction(select)

    select.on('select', event => {
      const { selected } = event

      // Unselect first
      const feature =
        layerVector_.getSource().getFeatures().find(f => f.get('selected'))
      if (feature) feature.set('selected', false)
      
      $info.classList.remove('show')
      
      selected.forEach(f => f.set('selected', true))

      if (selected.length) {
        const { title, location, gps, hours } = selected[0].getProperties()
        $info.innerHTML = 
        `<table>
          ${title ? `<tr><td>${T_('Name')}</td><td>${title}</td></tr>` : ''}
          ${location ? `<tr><td>${T_('Where')}</td><td>${location}</td></tr>` : ''}
          ${gps ? `<tr><td>GPS</td><td>${gps}</td></tr>` : ''}
          ${hours ? `<tr><td>${T_('Hours')}</td><td>${hours}</td></tr>` : ''}
        </table>`

        $info.classList.add('show')
      }
    })

    // Center view in marker when panel is touched
    $info.onclick = () => {
      const feature =
        layerVector_.getSource().getFeatures().find(f => f.get('selected'))
    
      if (feature) {
        ol_.getView().fit(feature.getGeometry().getExtent())
        ol_.getView().setZoom(14)
      }
    }

    ol_.on('click', event => {
      // Hide search input
      $input.classList.remove('show')
      $input.blur()

      // Unselect
      if (ol_.hasFeatureAtPixel(event.pixel)) return;
      const feature =
        layerVector_.getSource().getFeatures().find(f => f.get('selected'))
      if (feature) feature.set('selected', false)

      // Hide info panel
      $info.classList.remove('show')
    });
  }



  /**
   * Base style
   * 
   * @param {object} feature
   * @param {number} resolution
   */
  function get_base_style(feature, resolution) {
    const { selected } = feature.getProperties()

    resolution = selected ? 0.1 : 0.2; // fixed size for now
    const scale = 0.2 / resolution

    let styles = [
      new ol.style.Style({
        image: new ol.style.Icon({
          src: 'img/icon-map-bg.svg',
          scale,
          opacity: 0.7
        }),
        zIndex: 1
      }),
      new ol.style.Style({
        image: new ol.style.Icon({
          src: feature.getId() != 'me' ? 
            'img/icon-map-marker.svg' : 'img/icon-map-me.svg',
          scale
        }),
        zIndex: 3
      })
    ]

    if (selected)
      styles.push(
        new ol.style.Style({
          image: new ol.style.Icon({
            src: 'img/icon-map-selection.svg',
            scale
          }),
          zIndex: 2
        })
      )

    return styles
  }


  /**
   * Fetch Markets in a city
   * 
   * @param {string} city
   */
  function fetch_markets(city) {
    // Fetch and add Markets
    fetch(`${MARKETS_URL}${city}.json`)
    .then(response => response.json())
    .then(data => {
      const { features } = data
      
      if (!features) return alert(T_('No markets found in resource'))
      
      for (const f of features) {
        const { geometry, properties } = f || ''
        if (!geometry || !properties) continue

        const { coordinates: lonlat } = geometry
        if (!lonlat) continue

        set_market(lonlat, properties)
      }

      // Fit to Markets area
      ol_.getView().fit(layerVector_.getSource().getExtent())
    })
    .catch(err => alert(`${T_('Fetch error')} ${err}`))
  }


  /**
   * Set market
   * 
   * @param {array} lonlat
   * @param {object} properties
   */
  function set_market(lonlat, properties) {
    const { title, location, opening_hours, opening_hours_unclassified } = 
      properties

    // Parameters
    feature = new ol.Feature()
    feature.set('title', title)
    feature.set('location', location)
    feature.set('gps', lonlat.join(', '))
    feature.set('hours', opening_hours || opening_hours_unclassified)

    // GPS
    feature.setGeometry(new ol.geom.Point(
      ol.proj.transform(lonlat, 'EPSG:4326', 'EPSG:900913')))

    // Add to the map
    layerVector_.getSource().addFeature(feature)

  }

  //----------------------------------------------------------------------------
  return {
    load
  };
})();
