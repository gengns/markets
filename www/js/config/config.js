/**
  @file config.js

  @date 2019-8
  @author Génesis García Morilla <gengns@gmail.com>

  @description Basic configuration
*/


//============================================================================
// GENERAL PARAMETERS
//============================================================================
const MARKETS_URL = 'https://raw.githubusercontent.com/wo-ist-markt/' + 
  'wo-ist-markt.github.io/master/cities/'