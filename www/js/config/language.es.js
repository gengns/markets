﻿const language = {}
language.es = 
{
  "Markets in your city": "Mercados en tu ciudad",
  "user": "usuario",
  "password": "contraseña",
  "Login": "Login",
  "Wrong user and/or password. Please, try again": "Usuario y/o contraseña incorrectos. Porfavor, intentelo de nuevo",
  
  "Markets": "Mercados",
  "My Markets": "Mis Mercados",
  "Buy": "Comprar",
  "Sell": "Vender",
  "Share Basket": "Compartir Cesta",
  "Earnings": "Ganancias",
  "Help": "Ayuda",
  "Logout": "Cerrar Sesión",
  "Urban Markets": "Mercados Urbanos",

  "No markets found in resource": "No se han encontrado mercados en la fuente",
  "Fetch error": "Error al acceder a la fuente",
  "city": "ciudad",
  "Sorry, that city name is not avaliable":  "Lo sentimos, ese nombre de ciudad no está disponible",

  "Allow GPS access": "Active permisos GPS",
  "GPS not avaliable, did you switch on?": "GPS no disponible, ¿lo tiene activo?",

  "Name": "Nombre",
  "Where": "Donde",
  "Hours": "Horas"
}
