/**
  @file gquery.js

  @author Génesis García Morilla <gengns@gmail.com>
  @date 2019-08

  @description Utilities
*/


/**
 * Short selector like jQuery
 * 
 * @param {string} selector 
 */
function $(selector) {
  return document.querySelector(selector);
}


function $$(selector) {
  return document.querySelectorAll(selector);
}


Element.prototype.$ = function(selector) {
  return this.querySelector(selector);
}


Element.prototype.$$ = function(selector) {
  return this.querySelectorAll(selector);
}



/**
 * Translator
 */
let T = language[localStorage.language || 'en'] // we can set a config file by default
let T_ = txt => T && T[txt] ? T[txt] : txt;



/**
 * Gets GPS coordinates with HTML5
 *
 * @param {function} success
 * @param {function} failure
 */
function gps(success, failure) {
  navigator.geolocation.getCurrentPosition(
    pos => success(pos.coords.latitude, pos.coords.longitude),
    err => failure(err),
    { maximumAge: 0, timeout: 10000, enableHighAccuracy: true } // options
  )
}
