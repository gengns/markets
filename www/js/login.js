/**
  @file login.js
 
  @date 2019-8
  @author Génesis García Morilla <gengns@gmail.com>

  @description Access to the app
 */
const login = (() => {
  /**
   * Loads view
   */
  function load() {
    // Template
    const html =
    `<img src="img/logo.svg">

    <form>
      <h1>${T_('Markets in your city')}</h1>
      <input id="user" placeholder="${T_('user')} *" autofocus required>
      <input id="password" placeholder="${T_('password')} *" type="password" required>
      <button>${T_('Login')}</button>
    </form>

    <img src="img/gengns.svg">

    <select>
      <option value="en" selected>English</option>
      <option value="es">Español</option>
    </select>
    `


    // Render
    $('#app').innerHTML = `<div id="login">${html}</div>`


    // Selectors
    const $user = $('#user')
    const $password = $('#password')
    const $select = $('select')

    // Login
    $('form').onsubmit = e => {
      e.preventDefault()
      // The server should check the authorization
      if ($user.value == 'demo' && $password.value == 'demo') {
        sessionStorage.user = $user.value
        sessionStorage.password = $password.value
        place.load()
      } else {
        alert(T_('Wrong user and/or password. Please, try again'))
        $user.value = ''
        $password.value = ''
      }
    }

    // Set session user and password
    $user.value = sessionStorage.user || ''
    $password.value = sessionStorage.password || ''


    // Change language
    $select.onchange = e => {
      const str = e.target.value
      // Store
      localStorage.language = str
      // Set
      T = language[str]
      T_ = txt => T && T[txt] ? T[txt] : txt
      load()
    }

    // Set default language
    $select.value = localStorage.language || 'en'
  }

  //----------------------------------------------------------------------------
  return {
    load
  };
})();
