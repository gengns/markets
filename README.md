# Markets

**Markets** is a mobile app for fetching the markets information based on typed city or user’s location in a city. It uses the open API data from [wo is markt](https://github.com/wo-ist-markt/wo-ist-markt.github.io) (Where is market) project.

## Web
* Tested with Chromium/Chrome last version
* Go to `www` folder and type `http-server` to excute it in: localhost:8080
* Login: demo, demo
* You can resize the window, use GPS and so on

## Android app
* Tested with Chrome (Webview) last version in real device (Android with developer options)
* Go to main folder and type:
```
cordova plugin add cordova-plugin-geolocation && cordova platform add android && cordova run --debug --device
```
* Login: demo, demo

### Android app dependencies  
If you want to generate an Android app first of all you need to install npm, Node, Android Studio, Gradle, Cordova and set your enviroment variables. It's easier than you think. Don't you have it yet? Want to do it now? 😊

#### Install npm and Node
1. `sudo apt install npm`
2. `npm i n -g && n latest` 

#### Install Android Studio, Gradle and Cordova
1. [Android Studio](https://developer.android.com/studio)
2. [Gradle](https://gradle.org/install/)  
3. `npm i -g cordova`

#### Access from your system (systems environment variables)
1. `nano ~/.bashrc`
2. Add 
```
# Android
export ANDROID_SDK_ROOT=~/Android/Sdk
export PATH=${PATH}:~/Android/Sdk/tools:~/Android/Sdk/platform-tools

# Gradle
export PATH=$PATH:/opt/gradle/gradle-6.1.1/bin
```
3. `source ~/.bashrc`

Please drop me an email if you have any problems. Happy to help! ❤